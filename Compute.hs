module Compute where

  import           Tools
  import           Vector
  import           Configuration

  -- Constante
  mu0 :: 
         Double
  mu0
    = 4*pi*(10**(-7))

  -- Donne le dl pour un certain angle, pour la bobine A
  dlA :: 
         Settings
      -> Angle
      -> Vector3
  dlA s a
    = let
        Radians t = toRadians a 
        Distance radius = tXradiusAB s
      in
        ((2*pi*radius / (tXintegralPrecision s)) :: Double) |* (Vector3 0 (sin t) (cos t))

  -- Donne le r' pour un certain angle, pour la bobine A
  rA' ::
         Settings
      -> Angle
      -> Vector3
  rA' s a
    = let   
        Radians t = toRadians a
        Distance radius = tXradiusAB s
        Distance lA = tXdistanceAC s 
      in 
        Vector3 (-lA) (cos t * radius) (- sin t * radius)

  -- Donne la valeur du champ B pour un certain angle, pour la bobine A (intégrale)
  valA :: 
          Settings 
       -> Vector3
       -> Angle
       -> Vector3
  valA s r a
    = let
        Current i = tXcurrent s
      in 
        (i/(vmag (r - rA' s a))^3) |* ((dlA s a) `vcross` (r - rA' s a)) {- I/(r-r')^3 * (dl x (r-r') -}

  -- Donne la valeur du champ B, pour la bobine A (Biot-Savart)
  dBA :: 
         Settings 
      -> Vector3 
      -> Vector3
  dBA s r
    = (mu0/(4*pi) :: Double) |* (sum [let {- int_C{ -}
                                        a' = (Radians $ a*2*pi/(tXintegralPrecision s))
                                      in (valA s r a') | a <- [0..(tXintegralPrecision s)-1] {- I/(r-r')^3 * (dl x (r-r') -}
                                     ]) {- } -}

  -- Donne le dl pour un certain angle, pour la bobine B
  dlB :: 
         Settings
      -> Angle
      -> Vector3
  dlB s a
    = let
        Radians t = toRadians a 
        Distance radius = tXradiusAB s
        Radians gamma = toRadians $ tXgamma s 
        Radians delta = toRadians $ tXdelta s 
      in
        ((2*pi*radius / (tXintegralPrecision s)) :: Double) |* (Vector3 (- cos gamma * sin gamma * cos t - cos delta * sin delta * sin t) (sin t) (cos t))

  -- Donne le r' pour un certain angle, pour la bobine B
  rB' ::
         Settings
      -> Angle
      -> Vector3
  rB' s a
    = let   
        Radians t = toRadians a
        Distance radius = tXradiusAB s
        Distance lB = tXdistanceCB s 
        Radians gamma = toRadians $ tXgamma s 
        Radians delta = toRadians $ tXdelta s 
        bX = -lB
        bY = cos t * radius
        bZ = - sin t * radius
      in 
        - Vector3 (bX - cos gamma * sin gamma * bZ - cos delta * sin delta * bY) (bY - sin delta * sin delta * bY) (bZ - sin gamma * sin gamma * bZ) -- car champ B opposé

  -- Donne la valeur du champ B pour un certain angle, pour la bobine B (intégrale)
  valB ::
          Settings
       -> Vector3
       -> Angle
       -> Vector3
  valB s r a
    = let
        Current i = tXcurrent s
      in 
        (- i/(vmag (r - rB' s a))^3) |* ((dlB s a) `vcross` (r - rB' s a)) {- -I/(r-r')^3 * (dl x (r-r') -} -- -I car courant inversé dans cette bobine

  -- Donne la valeur du champ B, pour la bobine B (Biot-Savart)
  dBB :: 
         Settings
      -> Vector3
      -> Vector3
  dBB s r
    = (mu0/(4*pi)::Double) |* (sum [let {- int_C{ -}
                                      a' = (Radians $ a*2*pi/(tXintegralPrecision s)) 
                                    in (valB s r a') | a <- [0..(tXintegralPrecision s)-1] {- -I/(r-r')^3 * (dl x (r-r') -}
                                   ]) {- } -}

  -- Donne la valeur réelle du champ B
  getMean ::
             Settings 
          -> Vector3
  getMean s
    = let 
        (Distance dy, Distance dz) = (tXdecCv s, tXdecCh s)
        Distance radius = tXradiusC s
        Radians alpha = toRadians $ tXalpha s 
        Radians beta = toRadians $ tXbeta s 
        nv x y z = Vector3 x (dy+y) (dz+z) 
      in 
        mean [mean [let 
                      t = a*2*pi/(tXintegralPrecision s)
                      d = a * radius / (tXmeanPrecision s)
                      bX = 0
                      bY = cos t * radius
                      bZ = sin t * radius
                      n = nv (- cos alpha * sin alpha * bZ - cos beta * sin beta * bY) (bY - sin beta * sin beta * bY) (bZ - sin alpha * sin alpha * bZ) 
                    in 
                      (dBA s n + dBB s n)::Vector3 | a <- [0..(tXintegralPrecision s)-1]
                    ] | p <- [0..(tXmeanPrecision s)-1]
              ]

  getConstant' ::
                 Settings
              -> Vector3
              -> Double
  getConstant' s m
    = let
        Distance radius = tXradiusAB s
        Current i = tXcurrent s
      in 
        radius * (v3x m) / (mu0*i)

  -- Donne la constante que l'on cherche à déterminer (B*r/(mu0*i))
  getConstant ::
                 Settings
              -> Double 
  getConstant s
    = getConstant' s (getMean s)