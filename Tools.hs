{-# LANGUAGE DeriveDataTypeable #-}

module Tools where

  import           Vector
  import           Data.Fixed
  import           Text.JSON
  import           Text.JSON.Generic

  -- Donne le a mod b si a /= b, sinon donne a
  mod'' :: (Real a, Fractional a) => 
           a
        -> a
        -> a
  a `mod''` b
    = let
        am = a `mod'` b
      in
        if
          am == 0.0
        then
          b
        else
          am

  data Angle
    = Degrees Double
    | Radians Double
    deriving (Data, Typeable)

  instance Show Angle where
    show (Degrees d)
      = show d ++ "°"
    show (Radians r)
      = show (r/pi) ++ "pi"

  toRadians ::
                Angle
             -> Angle
  toRadians (Degrees d)
    = Radians $ d/180*pi 
  toRadians (Radians r)
    = Radians r

  toDegrees ::
               Angle
            -> Angle
  toDegrees (Degrees d)
    = Degrees d
  toDegrees (Radians r)
    = Degrees $ r/pi*180

  doubleToAngle ::
                   Double
                -> Angle
  doubleToAngle d
    = Degrees d 

  instance Eq Angle where
    (Degrees a) == (Degrees b)
      = a `mod''` 360 == b `mod'` 360
    (Degrees a) == (Radians b)
      = (a/180*pi) `mod''` pi == b `mod''` pi
    (Radians a) == (Degrees b)
      = b == a 
    (Radians a) == (Radians b)
      = b `mod''` pi == a `mod''` pi

  instance Ord Angle where
    (Degrees a) <= (Degrees b)
      = a `mod''` 360 <= b `mod''` 360
    (Degrees a) <= (Radians b)
      = (a/180*pi) `mod''` pi <= b `mod''` pi
    (Radians a) <= (Degrees b)
      = (b*180/pi) `mod''` 360 <= a `mod''` 360
    (Radians a) <= (Radians b)
      = b `mod''` pi <= a `mod''` pi
   
  newtype Distance
    = Distance Double 
      deriving (Ord, Eq, Data, Typeable)

  instance Show Distance where
    show (Distance d)
      = show d

  doubleToDistance ::
                      Double
                   -> Distance
  doubleToDistance d
    = Distance d

  newtype Current
    = Current Double
      deriving (Ord, Eq, Data, Typeable)

  instance Show Current where
    show (Current d)
      = show d

  doubleToCurrent ::
                     Double
                  -> Current
  doubleToCurrent d
    = Current d

  mean ::
          [Vector3]
        -> Vector3
  mean s
    = (1/(fromIntegral (length s))) |* sum s