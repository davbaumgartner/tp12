{-# LANGUAGE DeriveDataTypeable #-}

module Configuration (Settings(..), loadSettings) where

  import           Tools
  import           Text.JSON
  import           Text.JSON.Generic

  data SettingsR
    = SettingsR { alpha :: Double
                , beta :: Double
                , gamma :: Double
                , delta :: Double
                , distanceAC :: Double
                , distanceCB :: Double
                , radiusAB :: Double
                , radiusC :: Double
                , decCv :: Double
                , decCh :: Double 
                , current :: Double
                , integralPrecision :: Double
                , meanPrecision :: Double
                }
      deriving (Show, Data, Typeable)

  data Settings
    = Settings { tXalpha :: Angle
               , tXbeta :: Angle
               , tXgamma :: Angle
               , tXdelta :: Angle
               , tXdistanceAC :: Distance
               , tXdistanceCB :: Distance
               , tXradiusAB :: Distance
               , tXradiusC :: Distance
               , tXdecCv :: Distance
               , tXdecCh :: Distance
               , tXcurrent :: Current
               , tXintegralPrecision :: Double
               , tXmeanPrecision :: Double
                }
      deriving (Show)

  settingsRtoSettings ::
                         SettingsR 
                      -> Settings
  settingsRtoSettings d
    = Settings { tXalpha = doubleToAngle $ alpha d
               , tXbeta = doubleToAngle $ beta d
               , tXgamma = doubleToAngle $ gamma d
               , tXdelta = doubleToAngle $ delta d
               , tXdistanceAC = doubleToDistance $ distanceAC d
               , tXdistanceCB = doubleToDistance $ distanceCB d
               , tXradiusAB = doubleToDistance $ radiusAB d
               , tXradiusC = doubleToDistance $ radiusC d
               , tXdecCv = doubleToDistance $ decCv d
               , tXdecCh = doubleToDistance $ decCh d
               , tXcurrent = doubleToCurrent $ current d
               , tXintegralPrecision = integralPrecision d
               , tXmeanPrecision = meanPrecision d
               }

  loadSettings ::
                  FilePath
               -> IO Settings
  loadSettings p
    =   readFile p 
    >>= return . settingsRtoSettings . decodeJSON