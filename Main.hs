module Main where

  import           System.Environment

  import           Configuration
  import           Compute

  main ::
          IO ()
  main
    = do fn <- fmap head getArgs
         s <- loadSettings fn
         putStrLn "Déterminant avec la configuration:"
         print s
         let b = getMean s
         putStrLn "B déterminé:"
         print b
         putStrLn "Constante déterminée:"
         print $ getConstant' s b